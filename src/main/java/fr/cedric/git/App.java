package fr.cedric.git;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.URIish;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Collections;

@Slf4j
public class App {

    private static final String WORKING_DIR = "/home/cedric/dev/data/projects/test";

    private static final String PATH_REPO_A    = WORKING_DIR + "/remote/repoA";
    private static final String PATH_REPO_B    = WORKING_DIR + "/remote/repoB";
    private static final String PATH_MONO_REPO = WORKING_DIR + "/remote/monoRepo";

    public static void main(String[] args)
            throws IOException, GitAPIException, URISyntaxException {
        GitService gitService = new GitService();

        // 1. => Création d'un repository A avec plusieurs branches

        Git gitRepositoryA = initRepo(gitService, PATH_REPO_A, "Repository A");

        // 2. => Création d'un repository B avec plusieurs branches

        Git gitRepositoryB = initRepo(gitService, PATH_REPO_B, "Repository B");

        // 3. => Création d'un repository C à partir des repositories A et B

        Git gitMonoRepo = gitService.init(PATH_MONO_REPO);
        addRemotesAndFetch(gitMonoRepo);

        File readmeFile = FileUtils.createTextFile(PATH_MONO_REPO + "/README.md", "# " + "MONO REPO\n\nLine 1");
        gitService.commit(gitMonoRepo, "README.md", "add README.md");

        new File(PATH_MONO_REPO + "/repoA").mkdir();
        new File(PATH_MONO_REPO + "/repoB").mkdir();

        // rejouer tous les commits présents sur le repoA
        // si possible garder les commits IDS

        // dans un premier temps récupérer l'id du premier commit sur le repoA et pour la branche master

        Repository repository = gitService.buildRepository(PATH_REPO_A + "/.git");

        // FIXME : itération par ordre chrono (par défaut c'est l'ordre inverse)
        Iterable<RevCommit> logs = gitRepositoryA.log()
                .add(repository.resolve("refs/heads/master"))
                .call();

        RevCommit firstCommit = logs.iterator().next();
        log.info("first commit : {} - {} : {}",
                firstCommit.getId().getName().substring(0, 7),
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(firstCommit.getAuthorIdent().getWhen()),
                firstCommit.getShortMessage());

    }

    /**
     * Initialise un repository GIT à partir d'un dossier vide :
     * <ul>
     *     <li>Commit 1 : ajout du README.md</li>
     *     <li>Commit 2 : mise à jour du README.md</li>
     *     <li>Commit 3 : ajout d'un fichier dans un sous dossier / commit avec un autre auteur</li>
     *     <li>Création d'une nouvelle branche à partir du commit 3</li>
     *     <li>Modification d'un fichier dans la nouvelle branche</li>
     * </ul>
     */
    private static Git initRepo(GitService gitService, String repoPath, String projectName)
            throws GitAPIException, IOException {
        Git git = gitService.init(repoPath);

        String readmeFilename = "README.md";
        File readmeFile = FileUtils.createTextFile(repoPath + "/" + readmeFilename, "# " + projectName + "\n\nLine 1");
        gitService.commit(git, readmeFilename, "add README.md");

        org.apache.commons.io.FileUtils.writeLines(readmeFile, Collections.singleton("Line 2"), true);
        gitService.commit(git, readmeFilename, "update README.md => add Line 2");

        new File(repoPath + "/directory").mkdir();
        File sampleFile = FileUtils.createTextFile(repoPath + "/directory/sample.txt", "Sample file");
        gitService.commit(git, "directory/sample.txt", "add sample.txt",
                "John Doe", "jdoe.ext@galerieslafayette.com");

        git.checkout()
                .setCreateBranch(true)
                .setName("develop-bhv")
                .call();

        sampleFile.delete();
        sampleFile = FileUtils.createTextFile(repoPath + "/directory/sample.txt", "Sample file modified");
        gitService.commit(git, "directory/sample.txt", "update sample.txt");

        return git;
    }

    private static void addRemotesAndFetch(Git git)
            throws GitAPIException, URISyntaxException {
        git.remoteAdd()
                .setName("repoA")
                .setUri(new URIish(PATH_REPO_A + "/.git"))
                .call();

        git.fetch()
                .setRemote("repoA")
                .setRemoveDeletedRefs(true)
                .call();

        git.remoteAdd()
                .setName("repoB")
                .setUri(new URIish(PATH_REPO_B + "/.git"))
                .call();

        git.fetch()
                .setRemote("repoB")
                .setRemoveDeletedRefs(true)
                .call();
    }

}
