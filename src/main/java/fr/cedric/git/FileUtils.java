package fr.cedric.git;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@UtilityClass
@Slf4j
public class FileUtils {

    public static File deleteAndCreateDirectory(String path)
            throws IOException {
        File file = new File(path);
        if (file.exists()) {
            org.apache.commons.io.FileUtils.deleteDirectory(file);

            log.info("Directory {} has been deleted ...", path);
        }
        file.mkdir();
        log.info("Directory {} has been created ...", path);
        return file;
    }

    public static File createTextFile(String path, String text)
            throws IOException {
        File file = new File(path);
        org.apache.commons.io.FileUtils.writeStringToFile(file, text, StandardCharsets.UTF_8);
        return file;
    }

}
