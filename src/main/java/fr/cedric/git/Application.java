package fr.cedric.git;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

@Slf4j
public class Application {


    private static final String GIT_DIR = "/home/cedric/dev/data/projects/test/ecom-clients/.git";

    private static final String WORK_BRANCH = "refs/heads/master"
            // "refs/heads/develop-bhv"
            ;

    private static final String MONO_REPO_PATH = "/home/cedric/dev/data/projects/test/repo-test";

    public static void main(String[] args)
            throws IOException, GitAPIException {
        GitService gitService = new GitService();
        Repository repository = gitService.buildRepository(GIT_DIR);
        Git inputGit = new Git(repository);

        // initialise un nouveau repository

        Git outputGit = gitService.init(MONO_REPO_PATH);

        // affiche les branches

        printBranches(inputGit);

        // affiche les logs d'une branche donnée

        printLogs(inputGit, repository);

        // crée un commit

        fr.cedric.git.FileUtils.createTextFile(MONO_REPO_PATH + "/hello-world.txt", "Hello World :)");

        gitService.commit(outputGit, ".", "first commit");

        // lire un commit
    }

    private static void printBranches(Git git)
            throws GitAPIException {
        List<Ref> branches = git.branchList()
                .setListMode(ListBranchCommand.ListMode.ALL)
                .call();

        branches.stream().limit(10).forEach(
                ref-> log.info("Branche '{}' ({})",
                        ref.getName(),
                        ref.getObjectId().getName().substring(0, 7)));
    }

    private static void printLogs(Git git, Repository repository)
            throws GitAPIException, IOException {
        Iterable<RevCommit> commits = git.log()
                .add(repository.resolve(WORK_BRANCH))
                .call();

        commits.forEach(commit -> log.info("{} : {} : {} : {}",
                "", // belongToBranches(git, commit.getId().getName()),
                commit.getId().getName().substring(0, 7),
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(commit.getAuthorIdent().getWhen()),
                commit.getShortMessage()));
    }

    private static void optimizePrintLogs() {
        // TODO Reducing memory usage with RevWalk
    }

    @SneakyThrows
    private static String belongToBranches(Git git, String commitId) {
        return String.join(", ", git.nameRev()
                .addPrefix("refs/head")
                .add(ObjectId.fromString(commitId))
                .call()
                .values());
    }

}
