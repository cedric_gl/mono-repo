package fr.cedric.git;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.CommitCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import java.io.File;
import java.io.IOException;

@Slf4j
public class GitService {

    // TODO : encapsuler dans le service le retour de cette méthode
    public Git init(String repoPath)
            throws GitAPIException, IOException {
        File file = FileUtils.deleteAndCreateDirectory(repoPath);

        try {
            return Git.init()
                    .setDirectory(file)
                    .call();
        } finally {
            log.info("Git repo initialized !");
        }
    }

    public void commit(Git git, String filepattern, String message)
            throws GitAPIException {
        commit(git, filepattern, message, null, null);
    }

    public void commit(Git git, String filepattern, String message, String authorName, String authorEmail)
            throws GitAPIException {
        git.add().addFilepattern(filepattern).call();
        CommitCommand commitCommand = git.commit().setMessage(message);
        if (null != authorName && null != authorEmail) {
            commitCommand.setAuthor(authorName, authorEmail);
        }
        RevCommit commit = commitCommand.call();

        log.info("Commit succes : {} : {}", commit.getId().getName(), commit.getShortMessage());
    }

    public Repository buildRepository(String gitDir)
            throws IOException {
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        return builder.setGitDir(new File(gitDir))
                .readEnvironment() // scan environment GIT_* variables
                .findGitDir() // scan up the file system tree
                .setMustExist(true)
                .build();
    }

}
